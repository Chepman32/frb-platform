import { motion } from "framer-motion";
import React, { useEffect, useState } from "react";
import RemoveIcon from "../assets/remove.png"
import Edit from "../assets/edit.png"
import { Link } from "react-router-dom";
import { app } from "../base";
const db = app.firestore()
export const VideosItem = ({ isTable, video, setIsModal, setIsRemoveModal, setChoosed, currentAlbum }) => {
  const [tags, setTags] = useState([])
  useEffect(() => {
    const unmount = db.collection("tags").onSnapshot((snapshot) => {
      const tempTags = []
      snapshot.forEach(doc =>{
        tempTags.push({...doc.data(), id: doc.id, name: doc.data().name})
      })
      const filtered = tempTags.filter(t => t.items.includes(video.url))
      setTags(filtered)
    })
    return unmount
  }, [])
  return (
    <motion.div className={!isTable ? "item" : "itemTable"} key={video.name +  video.url} layout >
            <video src={video.url} poster={video.previewUrl ? video.previewUrl : ""} onClick={() => {
                  setChoosed(video)
                  tags.forEach((t) => (t.items.forEach(i => console.log(i.includes("https://firebasestorage.googleapis.com/v0/b/frb-platform.appspot.com/o/videos%2Ffile_example_MP4_480_1_5MG.mp4?alt=media&token=63eac5b4-8740-4965-a709-2f694c1a6899")))))
                }} />
            <div style={{alignSelf: "center"}} >
            <h3>{video.name} </h3>
            <p>{video.desc || ""} </p>
            <div >
            Теги: { tags && tags.map((tag) => tag !== null && <Link key={tag.name} to={`/tags/${tag.name}`}>{ tag.name }{tags.length > 1 && ","} &nbsp; </Link>)}
            </div>
            </div>
            {
              currentAlbum !== "All videos" ?
              <div className="album_tools" style={{
                flexDirection: isTable ? "column" : "row",
                justifyContent: isTable ? "space-between" : "flex-end",
                alignItems: "center",
                }} >
                  <img src={Edit} onClick={() => {
                    setChoosed(video)
                    setIsModal(true)
                  }} alt="Edit" />
                  <img className="removeIcon" src={RemoveIcon} onClick={() => {
                    setChoosed(video)
                    setIsRemoveModal(true)
                  }}
                  alt="Remove" />
              </div>
              :
              <div/>
            }

          </motion.div>
  );
};
