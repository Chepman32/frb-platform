import React from "react";
import CrossIcon from "../assets/cross.png";
export const TagsItem = ({ text, handler }) => {
  return (
    <div className="tagsItem" onClick={() => handler ? handler() : {}} >
      <h4>{text} </h4>
      <img src={CrossIcon} alt="remove" />
    </div>
  );
};
