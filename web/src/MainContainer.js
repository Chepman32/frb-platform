import React from "react"
import { Link } from "react-router-dom"
import Logo from "./assets/logo.png"
export const MainContainer = ({children}) => {
  return (
    <div className="mainContainer">
      <header className="mainContainer__header">
        <Link to="/">
        <div className="mainContainer__logo tooltip" style={{height: "100%"}}>
        <img src={Logo} alt="logo" style={{width: "10%", height: "10%"}} />
        <span class="tooltiptext">На главную</span>
        </div>
        </Link>
      </header>
      <div className="mainContainer__content">
      {
        children
      }
      </div>
    </div>
  )
}