import { motion } from "framer-motion"
import React, { useState } from "react"
import { useEffect } from "react"
import CrossIcon from "./assets/cross.png"
import { app } from "./base"
const storage = app.storage()
const db = app.firestore()

export const NewAlbum = ({setVisible}) => {
  const [name, setName] = useState("")
  const [preview, setPreview] = useState()
  
  useEffect(() => {
    preview && console.log(createPreview(preview))
  }, [preview])
  const onAlbumCreate = async () => {
    if(!name) {
      alert("Введите название")
      return
    }
    if(!preview) {
      alert("Выберите изображение для обложки")
      return
    }
    const storageRef = storage.ref()
      const previewRef =  storageRef.child(`images/${preview.name}`)
      await previewRef.put(preview)
    const allVideosRef = db.collection('albums').doc('All videos')
    allVideosRef.get()
  .then(async (docSnapshot) => {
    if (docSnapshot.exists) {
      allVideosRef.onSnapshot(async (doc) => {
        db.collection("albums").doc(name).set({
          name,
          cover: preview ? await previewRef.getDownloadURL() : ""
        })
      });
    } else {
      allVideosRef.set({
        name: 'All videos',
        cover: "https://firebasestorage.googleapis.com/v0/b/frb-platform.appspot.com/o/images%2Fcovers%2Falbums%2Fimages%20(3).jfif?alt=media&token=39a4fc09-bd4c-449c-8845-69791e13fffa"
      })
      db.collection("albums").doc(name).set({
        name,
        cover: preview ? await previewRef.getDownloadURL() : ""
      })
    }
});
    setName("")
    setVisible(false)
  }

  const modal = React.useRef()
  const clickOutside = (e) => {
    if(e.target.current !== modal) {
      setVisible(false)
    }
  }

  const createPreview = (obj) => obj && URL.createObjectURL(obj)
  
  return (
    <motion.div className="backdrop"
    initial={{opacity: 0}}
    animate={{opacity: 1}}
    transition={{delay: 0.1}} onClick={(e) => {
      e.preventDefault()
      clickOutside(e)
    }}>
      <div
      className="editModal"
      ref={modal}
      onClick={(e) => e.stopPropagation()} >
        <div className="editModal__header">New album</div>
        <div className="editModal__container">
      <input type="text" value={name} onChange={(e) => setName(e.target.value)} placeholder="Название"
      style={{width: "100%", fontSize: "1.4rem"}} />
        <div style={{margin: "0 50px", display: "flex", flexDirection: "column", alignItems: "center"}} >
        <label>
          <input type="file" accept="image/*" onChange={(e) => setPreview(e.target.files[0])} />
          <i className="fa fa-cloud-upload" />{!preview && "Attach image file"}
        </label>
        {
          preview && <div className="editModal__thumbnail" >
          <img className="editModal__thumbnail__image" src={createPreview(preview)} style={{width: "300px", height: "auto"}}/>
          <img src={CrossIcon} className="editModal__removeThumbnail" onClick={() => setPreview(null)}/>
          </div>
        }
        <span>{preview && preview.name} </span>
        </div>
        <div style={{display: "flex", justifyContent: "flex-end", alignItems: "center", marginTop: "60px"}}>
  <button className="saveBtn" onClick={() => onAlbumCreate()}>Save</button>
  <button className="cancelBtn" onClick={() => setVisible(false)}>Cancel</button>
  </div></div>
    </div>
    </motion.div>
  )
}