import { motion } from "framer-motion";
import React, { useState, useEffect } from "react";
import firebase from "firebase";
import CrossIcon from "./assets/cross.png";
import { Progressbar } from "./Progressbar";
import { app } from "./base";
import { TagsItem } from "./components/TagsItem";
import { Dropdown } from "./Dropdown";
const storage = app.storage();
const db = app.firestore();

export const NewVideo = ({ currentAlbum, setVisible }) => {
  const [file, setFile] = useState(null);
  const [preview, setPreview] = useState(null);
  const [title, setTitle] = useState("");
  const [desc, setDesc] = useState("");
  const [tagName, setTagName] = useState("");
  const [tags, setTags] = useState([]);
  const [tagsData, setTagsData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [progress, setProgress] = useState(0);
  const [url, setUrl] = useState(null);
  useEffect(() => {
    const unmount = db.collection("tags").onSnapshot((snapshot) => {
      const tempTags = [];
      snapshot.forEach((doc) => {
        tempTags.push(doc.data().name);
      });
      setTagsData(tempTags);
      console.log(tags.some((item) => tagsData.includes(item)));
    });
    return unmount;
  }, [tags]);
  useEffect(() => {
    setTitle("");
    setDesc("");
  }, [url]);
  const onUpload = async () => {
    try {
      if (!title) {
        return alert("Введите название");
      }
      if (!desc) {
        return alert("Введите описание");
      }
      if (!file) {
        return alert("Выберите видеофайл");
      }
      if (!preview) {
        return alert("Выберите изображение для превью");
      }
      if (!tags.length) {
        return alert("Добавьте хотя бы один тег");
      }
      const storageRef = storage.ref();
      const fileRef = file ? storageRef.child(`videos/${file.name}`) : "";
      const previewRef = preview ? storageRef.child(`images/covers/videos/${preview.name}`) : "";
      setLoading(true);
      await fileRef.put(file);
      await previewRef.put(preview);
      fileRef.put(file).on(
        "state_changed",
        (snap) => {
          let percentage = (snap.bytesTransferred / snap.totalBytes) * 100;
          setProgress(percentage);
          console.log(progress);
        },
        (err) => {
          console.log(err);
        },
        async () => {
          let url = await storageRef.getDownloadURL();
          
          setUrl(url);
        }
      );
      db.collection("albums")
        .doc(currentAlbum)
        .update({
          videos: firebase.firestore.FieldValue.arrayUnion({
            name: title,
            desc,
            videoFileName: file.name,
            url: file && (await fileRef.getDownloadURL(fileRef)),
            imageFileName: preview.name,
            previewUrl: previewRef && (await previewRef.getDownloadURL()),
            tags,
          }),
        });
        currentAlbum !== "All videos" && db.collection("albums")
        .doc("All videos")
        .update({
          videos: firebase.firestore.FieldValue.arrayUnion({
            name: title,
            desc,
            videoFileName: file.name,
            url: file && (await fileRef.getDownloadURL(fileRef)),
            imageFileName: preview.name,
            previewUrl: previewRef && (await previewRef.getDownloadURL()),
            parentAlbum: currentAlbum
          }),
        });
        for(let i = 0; i < tags.length; i++) {
          tags.some((item) => tagsData.includes(item))
            ? db
                .collection("tags")
                .doc(tags[i])
                .update({
                  name: tags[i].trim(),
                  items: firebase.firestore.FieldValue.arrayUnion(await fileRef.getDownloadURL(fileRef)),
                })
            : db
                .collection("tags")
                .doc(tags[i])
                .set({
                  name: tags[i].trim(),
                  items: firebase.firestore.FieldValue.arrayUnion(await fileRef.getDownloadURL(fileRef)),
                });
        }
      setLoading(false);
      setVisible(false);
    } catch (e) {
      console.log(e);
    }
  };

  const modal = React.useRef();
  const clickOutside = (e) => {
    if (e.target.current !== modal) {
      setVisible(false);
    }
  };

  const createPreview = (obj) => obj && URL.createObjectURL(obj);
  const addTag = (value) => {
    tagName !== "" && setTags([...tags, value.trim()]);
    setTagName("");
  };

  return (
    <motion.div
      className="backdrop"
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ delay: 0.1 }}
      onClick={(e) => clickOutside(e)}
    >
      <div
        className="editModal newVideo"
        ref={modal}
        onClick={(e) => e.stopPropagation()}
      >
        <div className="editModal__header">New video</div>
        <div className="editModal__container">
          <input
            type="text"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
            placeholder="Название"
            style={{ width: "100%", fontSize: "1.4rem" }}
          />
          <textarea
            rows={5}
            value={desc}
            onChange={(e) => setDesc(e.target.value)}
            placeholder="Описание"
            style={{ width: "100%", fontSize: "1.2rem" }}
          />
          <div style={{ display: "flex" }}>
            <input type="file" accept="video/*" />
          </div>
          <section>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <label>
                <input
                  type="file"
                  accept="video/*"
                  onChange={(e) => setFile(e.target.files[0])}
                />
                <i className="fa fa-cloud-upload" />
                {!file && "Attach video file"}
              </label>
              {file && (
                <div className="editModal__thumbnail">
                  <video
                    className="editModal__thumbnail__image"
                    src={createPreview(file)}
                  />
                  <img
                    src={CrossIcon}
                    className="editModal__removeThumbnail"
                    onClick={() => setFile(null)}
                  />
                </div>
              )}
              <p>{file && file.name} </p>
            </div>
            <div
              style={{
                margin: "0 50px",
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <label>
                <input
                  type="file"
                  accept="image/*"
                  onChange={(e) => setPreview(e.target.files[0])}
                />
                <i className="fa fa-cloud-upload" />
                {!preview && "Attach image file"}
              </label>
              {preview && (
                <div className="editModal__thumbnail">
                  <img
                    className="editModal__thumbnail__image"
                    src={createPreview(preview)}
                    style={{ width: "300px", height: "auto" }}
                  />
                  <img
                    src={CrossIcon}
                    className="editModal__removeThumbnail"
                    onClick={() => setPreview(null)}
                  />
                </div>
              )}
              <span>{preview && preview.name} </span>
            </div>
            <div className="tags__input">
              <input
                value={tagName}
                onChange={(e) => setTagName(e.target.value)}
                onKeyDown={(e) => {
                  e.code === "Enter" && addTag(tagName);
                }}
                placeholder="Название тега"
              />
              <h2 onClick={() => addTag(tagName)}>+</h2>
            </div>
            {tags &&
              tags.map((tag) => (
                <TagsItem
                  key={tag}
                  text={tag}
                  handler={() => console.log(`${tag} removed`)}
                />
              ))}
            {tagsData && (
              <Dropdown
                options={tagsData}
                onChange={(value) => setTags([...tags, value])}
                handler={(value) => {
                  addTag(value)
                  setTagName(value);
                }}
              />
            )}
          </section>
          <div
            style={{
              display: "flex",
              justifyContent: "flex-end",
              alignItems: "center",
              marginTop: "60px",
            }}
          >
            <button className="saveBtn" onClick={() => onUpload()}>
              Save
            </button>
            <button className="cancelBtn" onClick={() => {
            }}>
              Cancel
            </button>
          </div>
          { loading && <Progressbar percentage={progress} /> }
        </div>
      </div>
    </motion.div>
  );
};
