import React from "react"
import { useState } from "react/cjs/react.development"
import { EditAlbum } from "./EditAlbum"
import { HomeItem } from "./HomeItem"
import { NewAlbum } from "./NewAlbum"
export const Home = ({albums}) => {
  const [isModal, setIsModal] = useState(false)
  const [albumModal, setAlbumModal] = useState(false)
  const [choosed, setChoosed] = useState()
  return (
    <div className="home" >
    <section>
        {
          albums.map(album => <HomeItem album={album} key={album.id} handler={() => {
            setChoosed(album)
            setAlbumModal(true)
          }} />)
        }
      </section>
      <footer>
      <button onClick={() => setIsModal(true)} >Create a new album</button>
      </footer>
      {
            albumModal &&
            <EditAlbum
            albumInfo={{
              id: choosed.name,
              name: choosed.name,
            }}
            setVisible={setAlbumModal}
            currentAlbum={choosed} />
          }
      {
      isModal && <NewAlbum setVisible={setIsModal} />
      }
      
      </div>
  )
}