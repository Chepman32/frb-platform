import React, { useEffect, useState } from 'react';
import { app } from './base';
import { Route, Switch } from 'react-router';
import { Home } from './Home';
import { Album } from './Album';
import { MainContainer } from './MainContainer';
import { Tags } from './Tags';
const db = app.firestore()
function App() {
  const [albums, setAlbums] = useState([])
  useEffect(() => {
    const unmount = db.collection("albums").onSnapshot((snapshot) => {
      const tempAlbums = []
      snapshot.forEach(doc =>{
        tempAlbums.push({...doc.data(), id: doc.id})
      })
      setAlbums(tempAlbums)
    })
    return unmount
  }, [])
  return (
    <MainContainer>
      <Switch>
        <Route exact path="/" render={() => <Home albums={albums}/> } />
        <Route path="/tags/:name" render={() => <Tags/> } />
        <Route path="/:album" component={Album} />
      </Switch>
    </MainContainer>
  );
}

export default App;
