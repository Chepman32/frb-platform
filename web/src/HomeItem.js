import React, { useEffect, useState } from "react"
import { Link } from "react-router-dom"
import { motion } from "framer-motion"
import { app } from "./base"
import Remove from "./assets/remove.png"
import Edit from "./assets/edit.png"
import { RemoveModal } from "./RemoveModal"
const db = app.firestore()
export const HomeItem = ({album, handler}) => {
  const [albumModal, setAlbumModal] = useState(false)
  const [isRemoveModal, setIsRemoveModal] = useState(false)
  useEffect(() => {
  })

  const removeAlbum = (album) => {
    db.collection("albums").doc(album).delete().then(() => {
      console.log("deleted")
    }).catch = (err) => {
      console.log("error", err)
    }
  }

  return (
    <motion.div layout >
    <Link to={`/${album.id}`} key={album.id} >
            <div className="homeItem">
            <h3 style={{fontWeight: 500, color: "#000"}} >{album.name} </h3>
            <img className="homeItem__preview" src={album.cover} alt="cover" />
            {
              album.name !== "All videos" && <div className="album_tools" >
              <img style={{width: "32px", height: "32px"}} src={Remove} onClick={(e) => {
                e.preventDefault()
                setIsRemoveModal(true)
              }} alt="Remove" />
              <img className="toolIcon" src={Edit} onClick={(e) => {
                e.preventDefault()
                handler()
                setAlbumModal(true)
              }}/>
              </div>
            }
          </div>
          </Link>
          {
        isRemoveModal && <RemoveModal confirm={() => removeAlbum(album.id)} cancel={() => setIsRemoveModal(false)}/>
      }
          </motion.div>
  )
}