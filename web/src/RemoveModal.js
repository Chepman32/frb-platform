import { motion } from "framer-motion"
import React from "react"
import { app } from "./base"

export const RemoveModal = ({cancel, confirm, text}) => {
  
  const modal = React.useRef()
  const clickOutside = (e) => {
    if(e.target.current !== modal) {
      cancel()
    }
  }

  return (
    <motion.div className="backdrop"
    initial={{opacity: 0}}
    animate={{opacity: 1}}
    transition={{delay: 0.1}}
    onClick={(e) => {
      clickOutside(e)
    }}>
      <motion.div
      className="editModal"
      ref={modal} >
        <div style={{display: "flex", justifyContent: "flex-end", alignItems: "center", marginTop: "60px"}}>
  <button className="saveBtn deleteBtn" onClick={() => confirm()}>Delete</button>
  <button className="cancelBtn notDeleteBtn" onClick={() => cancel()}>Cancel</button>
  </div>
    </motion.div>
    </motion.div>
  )
}