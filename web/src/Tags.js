import React, { useEffect, useState } from "react"
import { useParams } from "react-router"
import { app } from "./base"
import TileIcon from "./assets/tile.png"
import ListIcon from "./assets/list.png"
import { VideosItem } from "./components/VideosItem"
const db = app.firestore()
export const Tags = () => {
  const [tagsData, setTagsData] = useState([]);
  const [tagName, setTagName] = useState("")
  const [videos, setVideos] = useState([])
  const [items, setItems] = useState([]);
  const [isTable, setIsTable] = useState(true)
  const { name } = useParams()
  useEffect(() => {
    const unmount = db.collection("tags").onSnapshot((snapshot) => {
      const tempTags = [];
      snapshot.forEach((doc) => {
        tempTags.push(doc.data().name);
      });
      setTagsData(tempTags);
    });
    return unmount;
  }, [tagName]);
  useEffect(() => {
    const unmount = db.collection("tags").doc(name).onSnapshot((doc) => {
      setItems(doc.data().items)
    })
    return unmount
  }, [])
  useEffect(() => {
    const unmount = db.collection("albums").doc("All videos").onSnapshot((doc) => {
      setVideos(doc.data().videos)
      const tempVideos = doc.data().videos.filter((vid) => items.find((i) => i === vid.url))
      setVideos(tempVideos)
      setTagName(name)
    })
    filter()
    return unmount
  }, [items, name, tagsData])
const filter = () => {
  var result1 = [
    {id:1, name:'Sandra', type:'user', username:'sandra'},
    {id:2, name:'John', type:'admin', username:'johnny2'},
    {id:3, name:'Peter', type:'user', username:'pete'},
    {id:4, name:'Bobby', type:'user', username:'be_bob'}
];

var result2 = [
    {id:2, name:'John', email:'johnny@example.com'},
    {id:4, name:'Bobby', email:'bobby@example.com'}
];

var props = ['id', 'name'];

var result = videos.filter(function(o1){
    // filter out (!) items in result2
    return !tagsData.some(function(o2){
        return o1.url === o2;          // assumes unique id
    });
}).map(function(o){
    // use reduce to make objects with only the required properties
    // and map to apply this to the filtered array as a whole
    return props.reduce(function(newo, name){
        newo[name] = o[name];
        return newo;
    }, {});
});
console.log(result)
}
  return (
    <>
    <header className="albumHeader" >
      <h1>#{tagName} </h1>
      <img src={isTable ? ListIcon : TileIcon} onClick={() => setIsTable(!isTable)} style={{width: "32px", height: "32px", margin: "20px 0", cursor: "pointer"}} alt="layout" />
    </header>
    <section className={!isTable ? "album" : "table"} >
    {
          videos.map((video) => <VideosItem key={video.url} video={video} isTable={isTable} />
          )
        }
      </section>
        </>
  )
}