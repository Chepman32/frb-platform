import React, { Component } from 'react'
import Select from 'react-select'

export const Dropdown = ({options, handler}) => {
  const values = options.map((value) => ({value, label: value}))
  return (
    <Select options={values} onChange={(value) => handler(value.value)} placeholder="Существующие теги" />
  )
}