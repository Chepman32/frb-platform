import { motion } from "framer-motion"
import React from "react"
export const Progressbar = ({percentage}) => {
  return (
    <motion.div className="progressbar"
    initial={{width: 0}} animate={{width: percentage + "%"}} style={{
     height: "25px",
     padding: "5px 0 5px  0",
     display: "flex",
     justifyContent: "center",
     alignItems: "center",
      background: "background: rgb(197, 87, 87)",
      fontSize: "20px",
    }}>
      {percentage.toFixed()}%
    </motion.div>
  )
}