import { motion } from "framer-motion"
import React, { useEffect, useState } from "react"
import firebase from "firebase";
import { Link } from "react-router-dom"
import CrossIcon from "./assets/cross.png"
import { app } from "./base"
import { TagsItem } from "./components/TagsItem"
const storage = app.storage()
const db = app.firestore()

export const EditModal = ({data, setVisible, currentAlbum, videos}) => {
  const [title, setTitle] = useState(data.name)
  const [desc, setDesc] = useState(data.desc)
  const [tagsData, setTagsData] = useState([]);
  const [allTags, setAllTags] = useState([])
  const [tagName, setTagName] = useState("")
  const [tags, setTags] = useState([data.tags])
  const [file, setFile] = useState(null)
  const [preview, setPreview] = useState(null)
  useEffect(() => {
    const unmount = db.collection("tags").onSnapshot((snapshot) => {
      const tempTags = []
      snapshot.forEach(doc =>{
        tempTags.push({...doc.data(), id: doc.id, name: doc.data().name})
      })
      setAllTags(tempTags)
      const filtered = tempTags.filter(t => t.items.includes(data.url))
      console.log(filtered)
      setTagsData(filtered)
    })
    return unmount
  }, [])
  const onFileChange = (e) => {
    setFile(e.target.files[0])
  }
  const onUpload = async () => {
    const storageRef = storage.ref()
    const fileRef = storageRef.child(`videos/${file.name}`)
    const previewRef =  preview ? storageRef.child(`images/${preview.name}`) : ""
    file && await fileRef.put(file)
    preview && await previewRef.put(preview)
    const newArr = [...videos]
    newArr[videos.indexOf(data)].url = file ? await fileRef.getDownloadURL() : data.url
    newArr[videos.indexOf(data)].previewUrl = preview ? await previewRef.getDownloadURL() : data.previewUrl
    newArr[videos.indexOf(data)].imageFileName = preview ? preview.name : data.imageFileName
    newArr[videos.indexOf(data)].videoFileName = file ? file.name : data.videoFileName
    newArr[videos.indexOf(data)].name = title
    newArr[videos.indexOf(data)].desc = desc
    newArr[videos.indexOf(data)].tags = []
    const albumRef = db.collection("albums").doc(currentAlbum)
    albumRef.update({
    videos: newArr
    })
    setVisible(false)
  }
  const addTag = () => {
    const tagRef = db.collection("tags").doc(tagName)
    setTags([...tags, tagName.trim()])
    tagRef.get()
  .then((docSnapshot) => {
    if (docSnapshot.exists) {
      tagRef.onSnapshot((doc) => {
        // do stuff with the data
      });
    } else {
      tagRef.set({
        name: tagName,
        items: firebase.firestore.FieldValue.arrayUnion(data.url)
      })
    }
});
    tagName !== "" && allTags.filter(t => t.items.includes(data.url)).length > 0
    ?
    db
    .collection("tags")
    .doc(tagName)
    .update({
      items: firebase.firestore.FieldValue.arrayUnion(data.url),
    }).then(() => setTagName(""))
    :
    db
    .collection("tags")
    .doc(tagName)
    .set({
      name: tagName,
      items: firebase.firestore.FieldValue.arrayUnion(data.url),
    }).then(() => setTagName(""))
  }
  const modal = React.useRef()
  const clickOutside = (e) => {
    if(e.target.current !== modal) {
      setVisible(false)
    }
  }

  const createPreview = (obj) => obj && URL.createObjectURL(obj)

  return (
    <motion.div className="backdrop"
    initial={{opacity: 0}}
    animate={{opacity: 1}}
    transition={{delay: 0.05}} onClick={(e) => {
      clickOutside(e)
    }}>
      <div
      className="editModal"
      ref={modal}
      onClick={(e) => e.stopPropagation()} >
      <div className="editModal__header">Edit {title} </div>
        <div className="editModal__container">
        <video
      className="modal__video"
      src={data.url}
      key={data.id + Math.random().toString()}
      controls
      poster={data.previewUrl ? data.previewUrl : ""}
/>
<input 
      type="text" 
      value={title} 
      onChange={(e) => setTitle(e.target.value)} 
      placeholder="Название"
      style={{width: "100%", marginBottom: "20px", fontSize: "1.4rem"}} />
  <textarea 
  rows={5} 
  value={desc} 
  onChange={(e) => setDesc(e.target.value)} 
  placeholder="Описание" 
  style={{width: "100%", fontSize: "1.2rem"}} />
  <div style={{display: "flex"}}>
  <input type="file" accept="video/*" onChange={onFileChange}/>
  </div>
  <section>
  <div style={{display: "flex", flexDirection: "column", alignItems: "center"}}>
        <label>
          <input type="file" accept="video/*" onChange={(e) => setFile(e.target.files[0])} />
          <i className="fa fa-cloud-upload" />{!file && "Attach video file"}
        </label>
        {
          file && <div className="editModal__thumbnail" >
          <video className="editModal__thumbnail__image" src={createPreview(file)} />
          <img src={CrossIcon} className="editModal__removeThumbnail" onClick={() => setFile(null)} alt="remove" />
          </div>
        }
        <p>{file && file.name} </p>
        </div>
  <div style={{margin: "0 50px", display: "flex", flexDirection: "column", alignItems: "center"}} >
        <label>
          <input type="file" accept="image/*" onChange={(e) => setPreview(e.target.files[0])} />
          <i className="fa fa-cloud-upload" />{!preview && "Attach image file"}
        </label>
        {
          preview && <div className="editModal__thumbnail" >
          <img className="editModal__thumbnail__image" src={createPreview(preview)} style={{width: "300px", height: "auto"}} alt="preview" />
          <img src={CrossIcon} className="editModal__removeThumbnail" onClick={() => setPreview(null)} alt="remove" />
          </div>
        }
        <span>{preview && preview.name} </span>
        </div>
  </section>
  <div className="tags__input" >
          <input value={tagName} onChange={(e) => setTagName(e.target.value)} onKeyDown={(e) => {
            e.code === "Enter" && tagName &&  addTag()
          }} placeholder="Название тега" />
          <h2 onClick={() => tagName && addTag()} >+</h2>
        </div>
  { tagsData && tagsData.map((tag) => <TagsItem key={tag.name} text={tag.name} handler={() => {
    db
    .collection("tags")
    .doc(tag.name)
    .update({
      items: firebase.firestore.FieldValue.arrayRemove(data.url),
    })
  }}/>) }
  <div style={{display: "flex", justifyContent: "flex-end", alignItems: "center", marginTop: "60px"}}>
  <button className="saveBtn" onClick={() => onUpload()}>Save</button>
  <button className="cancelBtn" onClick={() => setVisible(false)}>Cancel</button>
  </div>
        </div>
    </div>
    </motion.div>
  )
}