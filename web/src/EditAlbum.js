import { motion } from "framer-motion"
import React, { useState } from "react"
import CrossIcon from "./assets/cross.png"
import { app } from "./base"
const storage = app.storage()
const db = app.firestore()

export const EditAlbum = ({setVisible, currentAlbum, albumInfo}) => {
  const [title, setTitle] = useState(albumInfo.name)
  const [preview, setPreview] = useState(null)
  const modal = React.useRef()
  const onUpload = async () => {
    const storageRef = storage.ref()
    const previewRef =  preview ? storageRef.child(`images/covers/albums/${preview.name}`) : ""
    preview && await previewRef.put(preview)
    
    const albumRef = db.collection("albums").doc(currentAlbum.name)
    albumRef.get().then(async (doc) => {
      if (doc.exists) {
          db.collection("albums").doc(currentAlbum.name).update({
            ...doc.data(),
            name: title,
            cover: await previewRef.getDownloadURL()
          })
          db.collection("albums").doc("All videos").update({
            ...doc.data(),
            name: title,
            cover: await previewRef.getDownloadURL()
          })
      } else {
          console.log("No such document!");
      }
  }).catch((error) => {
      console.log("Error getting document:", error);
  });
  setVisible(false)
  }
  
  const clickOutside = (e) => {
    if(e.target.current !== modal) {
      setVisible(false)
    }
  }

  const createPreview = (obj) => obj && URL.createObjectURL(obj)

  return (
    <motion.div
      className="backdrop"
      initial={{opacity: 0}}
      animate={{opacity: 1}}
      transition={{delay: 0.1}} onClick={(e) => {
      e.preventDefault()
      clickOutside(e)
    }}
    >
      <div
      className="editModal"

      ref={modal}
      onClick={(e) => e.stopPropagation()}>
        <div className="editModal__header">Edit album {albumInfo.name} </div>
        <div className="editModal__container">
        <input type="text" value={title} onChange={(e) => setTitle(e.target.value)} placeholder="Название"
      style={{width: "100%", fontSize: "1.4rem"}} />
      <div style={{margin: "0 50px", display: "flex", flexDirection: "column", alignItems: "center"}} >
        <label>
          <input type="file" accept="image/*" onChange={(e) => setPreview(e.target.files[0])} />
          <i className="fa fa-cloud-upload" />{!preview && "Attach image file"}
        </label>
        {
          preview && <div className="editModal__thumbnail" >
          <img className="editModal__thumbnail__image" src={createPreview(preview)} style={{width: "300px", height: "auto"}}/>
          <img src={CrossIcon} className="editModal__removeThumbnail" onClick={() => setPreview(null)}/>
          </div>
        }
        <span>{preview && preview.name} </span>
        </div>
  <div style={{display: "flex", justifyContent: "flex-end", alignItems: "center", marginTop: "60px"}}>
  <button className="saveBtn" onClick={() => onUpload()}>Save</button>
  <button className="cancelBtn" onClick={() => setVisible(false)}>Cancel</button>
  </div>
        </div>
    </div>
    </motion.div>
  )
}