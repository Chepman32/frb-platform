import React, { useEffect, useState } from "react"
import { useParams, useRouteMatch } from "react-router"
import { Link } from "react-router-dom"
import firebase from "firebase"
import { motion } from "framer-motion"
import { app } from "./base"
import { NewVideo } from "./NewVideo"
import { EditModal } from "./EditModal"
import { RemoveModal }from "./RemoveModal"
import TileIcon from "./assets/tile.png"
import ListIcon from "./assets/list.png"
import { VideosItem } from "./components/VideosItem"
const db = app.firestore()
export const Album = () => {
  const [isModal, setIsModal] = useState(false)
  const [newVideoModal, setNewVideoModal] = useState(false)
  const [isRemoveModal, setIsRemoveModal] = useState(false)
  const [videos, setVideos] = useState([])
  const [tagsData, setTagsData] = useState([]);
  const [name, setName] = useState("")
  const [isTable, setIsTable] = useState(true)
  const [choosed, setChoosed] = useState()
  const {album} = useParams()
  useEffect(() => {
    const unmount = db.collection("albums").doc(album).onSnapshot((doc) => {
      doc.data().videos && setVideos(doc.data().videos)
        setName(doc.data().name)
    })
    return unmount
  }, [album])
  const onRemove = (obj) => {
    const albumRef = db.collection("albums").doc(album)
    albumRef.update({
      videos: firebase.firestore.FieldValue.arrayRemove(obj)
    })
    album !== "All videos" && db.collection("albums").doc("All videos")
    .update({
      videos: firebase.firestore.FieldValue.arrayRemove(obj)
    })
    setIsRemoveModal(false)
  }

  return (
    <>
    <header className="albumHeader" >
      <h1>{name} </h1>
      <img src={isTable ? ListIcon : TileIcon} onClick={() => setIsTable(!isTable)} style={{width: "32px", height: "32px", margin: "20px 0", cursor: "pointer"}} />
    </header>
    <section className={!isTable ? "album" : "table"} alt="layout" >
        {
          videos.map((video) => <VideosItem key={video.url} video={video} isTable={isTable} setChoosed={setChoosed} setIsModal={setIsModal} setIsRemoveModal={setIsRemoveModal} currentAlbum={album}/>
          )
        }
        {
          isModal &&
          <EditModal data={choosed} setVisible={setIsModal} currentAlbum={album} videos={videos} />
        }
      </section>
      <footer className="album__footer" >
      <button onClick={() => setNewVideoModal(true)}>Add new video</button>
      </footer>
      {
        newVideoModal && <NewVideo currentAlbum={album} setVisible={() => setNewVideoModal(false)}/>
      }
      {
        isRemoveModal && <RemoveModal confirm={() => onRemove(choosed)} cancel={() => setIsRemoveModal(false)}/>
      }
        </>
  )
}