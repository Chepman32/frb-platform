import React, { useState, useEffect, useRef } from "react"
import * as ScreenOrientation from 'expo-screen-orientation'
import { Dimensions, ScrollView, StyleSheet, Text, TouchableOpacity, Image, TextInput, Share, Button, View } from 'react-native'
import { Video } from 'expo-av'
import { setStatusBarHidden } from 'expo-status-bar'
import firebase from "firebase"
import VideoPlayer from 'expo-video-player'
import Hyperlink from 'react-native-hyperlink'
import { height, width } from './constants'
import { useUserContext } from './navigation/UserContext'
import { app } from './base'
import { useTheme } from "./theme/ThemeContext"
import { useNavigation } from "@react-navigation/core"
import { CustomTitle } from "./CustomTitle"
import { Relatives } from "./Relatives"
const { width: DEVICE_WIDTH, height: DEVICE_HEIGHT } = Dimensions.get("window");
const FONT_SIZE = 14;
const VIDEO_CONTAINER_HEIGHT = (DEVICE_HEIGHT * 2.0) / 5.0 - FONT_SIZE * 2;
const db = app.firestore()
const ExpoPlayer = ({videos, uri, tags, playNext, posterSrc, title, desc}) => {
  const [isFav, setIsFav] = useState(false)
  const [history, setHistory] = useState([])
  const { user } = useUserContext()
  const [favorites, setFavorites] = useState([])
  const [inFullscreen2, setInFullsreen2] = useState(false)
  const [orientation, setOrientation] = useState(1)
  const navigation = useNavigation()
  useEffect(() => {
    const unmount = user ? db.collection("usersCollection").doc(user.email)
    .onSnapshot((doc) => {
        setFavorites(doc.data().favorites)
    })
    : console.log("unmounted")
    return unmount
  }, [])
  useEffect(() => {
    const unmount = user ? db.collection("usersCollection").doc(user.email)
    .onSnapshot((doc) => {
        setHistory(doc.data().history)
    })
    : console.log("unmounted")
    return unmount
  }, [])
  useEffect(() => {
    navigation.setOptions({
      header: (() => <CustomTitle name={""} />)
    })
  }, [])
  useEffect(() => {
    getFav()
  }, [favorites])
  const refVideo2 = useRef(null)
  const refScrollView = useRef(null)
  function getFav() {
    try {
      const urls = favorites.map((fav) => fav.url)
        setIsFav(urls.includes(uri))
    }
    catch(e) {
      console.log("No user")
    }
  }
const addToHistory = () => {
  try {
    app.firestore().collection("usersCollection").doc(user.email).get().then(queryResult =>{
      const alldata = queryResult.data().favorites
      setFavorites(alldata)
      
     }).then(() => {
      db.collection("usersCollection").doc(user.email).set({
        favorites,
        history: [{
          name: title,
        desc,
        previewUrl: posterSrc,
        url: uri,
        }, ...history],
      })
      console.log("added to history")
    })
  }
  catch(e) {
    console.log("No user")
  }
}
const addToFav = async () => {
  try {
    app.firestore().collection("usersCollection").doc(user.email).get().then(queryResult =>{
      const alldata = queryResult.data().favorites
      setFavorites(alldata)
      
     }).then(() => {
      db.collection("usersCollection").doc(user.email).set({
        favorites:[{
          name: title,
        desc,
        previewUrl: posterSrc,
        url: uri,
        }, ...favorites],
        history,
      })
      console.log("added")
    })
  }
  catch(e) {
    navigation.navigate("Login")
  }
}
const removeFromFav = () => {
  try {

    user ? uri && db.collection("usersCollection").doc(user.email).update({
      favorites: firebase.firestore.FieldValue.arrayRemove({
        name: title,
        desc,
        previewUrl: posterSrc,
        url: uri,
      })
    }).then(() => {
      console.log("removed")
    })
    : alert("no user")
  }
  catch(e) {
    console.log(e)
  }
}
const onShare = async () => {
  try {
    const result = await Share.share({
      message: 'Exanple link',
    });
    if (result.action === Share.sharedAction) {
      if (result.activityType) {
        // shared with activity type of result.activityType
      } else {
        // shared
      }
    } else if (result.action === Share.dismissedAction) {
      // dismissed
    }
  } catch (error) {
    alert(error.message);
  }
};
const { colors } = useTheme()
const getOrientation = async () => {
  ScreenOrientation.addOrientationChangeListener( async () => {
    setOrientation(await ScreenOrientation.getOrientationAsync())
    console.log(orientation)
  })
}
useEffect(() => {
  getOrientation()
}, [])
  return (
    <ScrollView
      scrollEnabled={!inFullscreen2}
      ref={refScrollView}
      onContentSizeChange={() => {
        if (inFullscreen2) {
          refScrollView.current.scrollToEnd({ animated: true })
        }
      }}
      style={{...styles.container, backgroundColor: colors.background}}
      contentContainerStyle={styles.contentContainer}
    >
          <VideoPlayer
        videoProps={{
          shouldPlay: false,
          resizeMode: Video.RESIZE_MODE_CONTAIN,
          usePoster: true,
          posterSource: {
            uri: posterSrc
          },
          source: {
            uri,
          },
          ref: refVideo2,
        }}
        playbackCallback={(status) => {
          playNext !== null && status.didJustFinish && addToHistory() && console.log("finished")
        }}
        defaultControlsVisible={true}
        fullscreen={{
          inFullscreen: inFullscreen2,
          enterFullscreen: async () => {
            setStatusBarHidden(true, 'fade')
            setInFullsreen2(!inFullscreen2)
            await ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.LANDSCAPE_RIGHT)
            refVideo2.current.setStatusAsync({
              shouldPlay: false,
            })
          },
          exitFullscreen: async () => {
            switcher()
            setStatusBarHidden(false, 'fade')
            setInFullsreen2(!inFullscreen2)
            await ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.PORTRAIT_UP)
          },
        }}
        icon={{
          play: <Image source={require("./assets/play-icon.png")} style={{width: 64, height: 64}} />,
          pause: <Image source={require("./assets/pause-icon.png")} style={{width: 64, height: 64}} />
        }}
        animation={{
          fadeInDuration: 0,
          fadeOutDuration: 5000
        }}
        style={{
          videoBackgroundColor: 'black',
          height: inFullscreen2 ? Dimensions.get('window').width : VIDEO_CONTAINER_HEIGHT,
          width: inFullscreen2 ? Dimensions.get('window').height : DEVICE_WIDTH,
        }}
      />
      {
        !inFullscreen2 && <>
        <TouchableOpacity style={{width: "100%", paddingRight: 50, alignItems: "flex-end"}} onPress={() => !isFav ? addToFav() : removeFromFav()}>
          <Image source={!isFav ? require("./assets/fav.png") : require("./assets/addedToFav.png")} style={{width: 48, height: 48}} />
          </TouchableOpacity>
      <Relatives items={videos} info={{
        name: title,
        uri,
        previewUrl: posterSrc,
        tags,
      }} />
        <Text style={{...styles.nameText, color: colors.text}}>{title} </Text>
        <Hyperlink
    linkDefault
    injectViewProps={ url => ({
          style: { color: '#ff8400' },
          //any other props you wish to pass to the component
    }) }
  >
    <Text style={{...styles.descText, color: colors.text}}>You can pass props to clickable components matched by url.
        and this url looks blue {desc} </Text>
  </Hyperlink>
  {
    tags && tags.map((tag) => <TouchableOpacity key={tag} onPress={() => navigation.navigate("Tags", { tag })}>
      <Text key={tag} >#{tag} </Text>
    </TouchableOpacity>)
  }
  <View style={{ width, marginTop: 50, paddingRight: 50, alignItems: "flex-end" }}>
      <Button onPress={onShare} title="Share" />
    </View>
        </>
}   
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  container: {
    height,
    backgroundColor: '#FFF',
    flex: 1,
  },
  contentContainer: {
    paddingBottom: height * 0.1,
    alignItems: 'center',
    justifyContent: "space-between",
    paddingTop: 40,
  },
  text: {
    marginTop: 36,
    marginBottom: 12,
  },
  nameText: {
    marginTop: 40,
    fontSize: 35,
    fontWeight: "400"
  },
  descText: {
    marginVertical: 20,
    fontSize: 20,
    fontWeight: "400"
  },
  nextBtn: {
    width,
    height: 50,
    position: "absolute",
    bottom: "42%",
    left: width / 1.3,
    right: 0,
    zIndex: 2,
    padding: 10,
    fontSize: 35,
    fontWeight: "400",
    color: "#fff"
  },
  prevBtn: {
    height: 50,
    position: "absolute",
    bottom: "40%",
    left: width / 7,
    zIndex: 1,
    padding: 10,
    fontSize: 35,
    fontWeight: "400",
    color: "#fff"
  },
  url: {
    color: 'red',
    textDecorationLine: 'underline',
  },

  email: {
    textDecorationLine: 'underline',
  },

  text: {
    color: 'black',
    fontSize: 15,
  },

  phone: {
    color: 'blue',
    textDecorationLine: 'underline',
  },

  name: {
    color: 'red',
  },

  username: {
    color: 'green',
    fontWeight: 'bold'
  },

  magicNumber: {
    fontSize: 42,
    color: 'pink',
  },

  hashTag: {
    fontStyle: 'italic',
  },
  input: {
    width,
    marginBottom: 50,
    backgroundColor: "rgba(227,196,80,0.2)",
    padding: 10,
    fontSize: 30,
    fontWeight: "500"
  }
})

export default ExpoPlayer