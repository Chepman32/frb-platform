import React, { useEffect, useState } from "react"
import { StyleSheet, ScrollView, FlatList, View, SafeAreaView } from "react-native"
import { useNavigation } from "@react-navigation/core"
import { app } from "../base"
import Preview from "../Preview"
import { useTheme } from "../theme/ThemeContext"
const db = app.firestore()
export const Home = () => {
  const [isReady, setIsReady] = useState(false)
  const [albums, setAlbums] = useState([])
  const [selectedId, setSelectedId] = useState(null);
  useEffect(() => {
    const unmount = db.collection("albums").onSnapshot((snapshot) => {
      const tempAlbums = []
      snapshot.forEach(doc =>{
        tempAlbums.push({...doc.data(), id: doc.id})
      })
      setAlbums(tempAlbums)
    })
    return unmount
  }, [])
  const {colors} = useTheme()
  const navigation = useNavigation()
  const renderItem = ({item}) => (
    <View key={item.id} >
      <Preview key={item.id + Math.random().toString()}  info={item} callback={() => navigation.navigate("Playlist",
    {name: item.name, videos: item.videos})} />
      </View>
  )
  return (
    <SafeAreaView style={{...styles.container, backgroundColor: colors.background}}>
      <FlatList
        data={albums}
        renderItem={renderItem}
        keyExtractor={item => item.url}
        extraData={selectedId}
      />
    </SafeAreaView>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  cover: {
    width: "100%",
    height: 200,
  },
  text: {
    marginVertical: 40,
    fontSize: 30,
    fontWeight: "400"
  }
})