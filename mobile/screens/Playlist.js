import React, { useEffect, useState } from "react"
import { FlatList, SafeAreaView, StyleSheet } from "react-native"
import { Title } from "react-native-paper"
import { CustomTitle } from "../CustomTitle"
import Preview from "../Preview"
import { useTheme } from "../theme/ThemeContext"

export const Playlist = ({navigation, route}) => {
  const [selectedId, setSelectedId] = useState(null);
  const { params } = route
  const { name } = params
  const { videos } = params
  
  useEffect(() => {
    navigation.setOptions({
      header: (() => <CustomTitle name="" />)
    })
  }, [])
  const renderItem = ({item}) => (
    <Preview key={item.url} info={item} callback={() => navigation.navigate("PlaylistItem", {
      video: item,
      videos,
      autoPlay: true
    })} />
  )
  const { colors } = useTheme()
  return (
    <SafeAreaView style={{...styles.container, backgroundColor: colors.background}}>
      <Title>{name} </Title>
      <FlatList
        data={videos}
        renderItem={renderItem}
        keyExtractor={item => item.url}
        extraData={selectedId}
      />
    </SafeAreaView>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    marginVertical: 40,
    fontSize: 22,
    fontWeight: "400"
  },
  preview: {
    width: "20%",
    height: "15%"
  }
})