import React, { useEffect, useState } from "react"
import { View, StyleSheet, TextInput, Text, Button, ActivityIndicator } from "react-native"
import AsyncStorage from '@react-native-async-storage/async-storage';
import { app } from "../base"
import { height } from "../constants"
import { useUserContext } from "../navigation/UserContext"
import { useTheme } from "../theme/ThemeContext"
export const Login = ({navigation}) => {
    const [storedPass, setStoredPass] = useState("")
    const [storedEmail, setStoredEmail] = useState("")
    const [isLoading, setisLoading] = useState(false)
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const { isAuthenticated } = useUserContext()
    const { colors } = useTheme()
    useEffect(() => {
        getData()
    }, [])
    useEffect(() => {
        storeData({email, password})
    }, [email, password])
    const storeData = async (value) => {
        try {
          const jsonValue = JSON.stringify(value)
          await AsyncStorage.setItem('@storage_Key', jsonValue)
        } catch (e) {
          // saving error
        }
      }
      
const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('@storage_Key')
      jsonValue !== null && setEmail(JSON.parse(jsonValue).email)
      jsonValue !== null && setPassword(JSON.parse(jsonValue).password)
    } catch(e) {
      // error reading value
    }
  }
  
    const containerStyle = {
        flex: 1,
        paddingTop: height * 0.02,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: colors.background,
    };
   const signIn = async () => {
        app.auth().signInWithEmailAndPassword(email, password).then(async (user) => {
            setisLoading(true)
            isAuthenticated
        })
        .catch((err) => {
            alert(err)
            setisLoading(false)
        })
    }
    return isLoading ? (
        <View style={styles.container}>
            <ActivityIndicator color="red"/>
        </View>
    )
    :
    (
        <View style={containerStyle}>
            <Text style={{marginBottom: height * 0.05, fontSize: height * 0.05, fontWeight: "500", color: colors.text}} >Login screen</Text>
            <TextInput style={{...styles.input, color: colors.text}} value={email} onChangeText={setEmail} placeholder="email"/>
            <TextInput style={{...styles.input, color: colors.text}} value={password} onChangeText={setPassword} placeholder="password"/>
            <Button title="Login" style={styles.btn} color={colors.primary} onPress={()  => signIn()} />
            <Text style={{fontSize: height * 0.02, color: colors.text}} >Don't have an account?
            &nbsp;
            <Text onPress={() => navigation.navigate("Signup")} style={{fontSize: height * 0.02, fontWeight: "600",color: "#00ff33"}}>Sign up</Text></Text>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    input: {
        marginVertical: 30,
    },
    btn: {
        paddingTop:10,
    paddingBottom:30,
    backgroundColor:'#1E6738',
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#fff'
    }
})