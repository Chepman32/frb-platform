import React from "react"
import { StyleSheet, View, Text } from "react-native"
import { useTheme } from "./theme/ThemeContext"
export const CustomTitle = ({name}) => {
  const { colors } = useTheme()
  return (
    <View style={{...styles.title, backgroundColor: colors.background}}>
      <View style={{...styles.horizontalLine, backgroundColor: colors.navSlider}}/>
      <Text style={styles.text} >{name} </Text>
    </View>
  )
}
const styles = StyleSheet.create({

  title: {
    marginTop: 40,
    justifyContent: "space-between",
    alignItems: "center",
    paddingTop: 20,
    paddingBottom: 3,
  },
  text: {
    fontSize: 25,
    fontWeight: "600",
  },
  horizontalLine: {
    width: "25%",
    height: 7,
    marginBottom: 10,
    backgroundColor: "rgba(0, 0, 0, 0.2)",
    overflow: "hidden",
    borderRadius: 5
  }
})