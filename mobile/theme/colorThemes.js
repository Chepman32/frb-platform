// Light theme colors
export const lightColors = {
  background: '#FFFFFF',
  primary: '#512DA8',
  text: '#121212',
  error: '#D32F2F',
  navSlider: "#827c7c",
  tabBar: "#7c8bc4"
};

// Dark theme colors
export const darkColors = {
  background: '#121212',
  primary: '#B39DDB',
  text: '#FFFFFF',
  error: '#EF9A9A',
  navSlider: "#c9c2c1",
  tabBar: "#3c5dd6"
};