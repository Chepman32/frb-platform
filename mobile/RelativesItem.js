import { useNavigation } from '@react-navigation/core'
import React, { useEffect, useState } from 'react'
import { View, Image, Text, StyleSheet } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { height, width } from './constants'
import { useTheme } from './theme/ThemeContext'

export default function RelativesItem({info, videos, callback}) {

  const navigation = useNavigation()
  const {colors} = useTheme()
  return (
    <TouchableOpacity onPress={() => navigation.navigate("PlaylistItem", {
      video: info,
      videos,
      autoPlay: true
    })} style={styles.container} activeOpacity={0.8} >
      <Image source={{uri: info.previewUrl}} style={styles.placeholder} />
      <Text style={{...styles.smallText, color: colors.text}}>{info.name} </Text>
    </TouchableOpacity>
  )
}
const styles = StyleSheet.create({
  container: {
    width: width * 0.9,
    marginVertical: 20,
    justifyContent: "center",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 20,
    position: "relative"
  },
  placeholder: {
    width: width * 0.5,
    height: height * 0.2,
  },
  text: {
    width: width * 0.9,
    marginTop: 20,
    fontSize: 20,
    fontWeight: "400",
    textAlign: "center"
  },
  smallText: {
    width: width * 0.5,
    marginTop: 20,
    fontSize: 15,
    fontWeight: "600",
    textAlign: "center"
  },
})