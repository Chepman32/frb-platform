import { useTheme } from "@react-navigation/native"
import React, { useState, useEffect } from "react"
import { View, Text, Image, StyleSheet } from "react-native"
import { app } from "./base"
import { height, width } from "./constants"
import { useUserContext } from "./navigation/UserContext"
export const AccountInfo = () => {
  const { user, isAuthenticated } = useUserContext()
  const { colors } = useTheme()
  return (
    <View style={{...styles.container, backgroundColor: colors.background}}>
      <Image source={user && isAuthenticated && user.photoURL ? {uri: user.photoURL} : require("./assets/avatar-placeholder.jpg")} style={styles.photo} />
      <Text onPress={() => console.log(user.photoURL)} style={{...styles.name, color: colors.text}}>{user && user.displayName} </Text>
      <Text style={{color: colors.text}}>{user && user.email} </Text>
    </View>
  )
}
const styles = StyleSheet.create({
  container: {
    width: width * 0.95,
    marginTop: height * 0.03,
    marginBottom: height * 0.4,
    paddingVertical: 20,
    paddingRight: width * 0.1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#e8ebe9",
    borderRadius: 8,
  },
  name: {
    fontSize: 40,
    fontWeight: "500"
  },
  photo: {
    width: 60,
    height: 60,
    marginRight: 30,
    overflow: "hidden",
    borderRadius: 30
  }
})