import React from 'react';
import { Asset } from 'expo-asset';
import AppLoading from 'expo-app-loading';
import Splash from "./Splash"
import { SafeAreaProvider } from 'react-native-safe-area-context';
import * as Sentry from 'sentry-expo';
Sentry.init({
  dsn: 'https://1b4e06990ba14d089bb2e428d4bfbc52@o1026479.ingest.sentry.io/5992867',
  enableInExpoDevelopment: true,
  debug: true, // Sentry will try to print out useful debugging information if something goes wrong with sending an event. Set this to `false` in production.
});

export default class App extends React.Component {
  state = {
    isReady: false,
  };
  render() {
    if (!this.state.isReady) {
      return (
        <AppLoading
          startAsync={this._cacheResourcesAsync}
          onFinish={() => this.setState({ isReady: true })}
          onError={console.warn}
        />
      ); }

    return (
      <SafeAreaProvider>
      <Splash/>
    </SafeAreaProvider>
    );
  }

  async _cacheResourcesAsync() {
    const images = [require('./assets/200px.png')];

    const cacheImages = images.map(image => {
      return Asset.fromModule(image).downloadAsync();
    }); 
    return Promise.all(cacheImages);
  }
}