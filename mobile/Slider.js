//React Native Slider
//https://aboutreact.com/react-native-slider/

//import React in our code
import React, {useState} from 'react';

//import all the components we are going to use
import {View, Text, SafeAreaView, StyleSheet} from 'react-native';

import Slider from '@react-native-community/slider';
import { width } from './constants';

const Progressbar = ({value, handler}) => {
  const [sliderValue, setSliderValue] = useState(value || 0);

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.container}>
        {/*Text to show slider value*/}

        {/*Slider with max, min, step and initial value*/}
        <Slider
        onSlidingComplete={() => handler(sliderValue)}
          maximumValue={100}
          minimumValue={0}
          minimumTrackTintColor="#307ecc"
          maximumTrackTintColor="#000000"
          step={1}
          value={value || 0}
          onValueChange={(sliderValue) => {
            setSliderValue(sliderValue)
          }}
          style={{width: width * 0.8}}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
  },
});

export default Progressbar;
