import React, { useEffect, useRef } from 'react'
import {AppearanceProvider, useColorScheme} from 'react-native-appearance';
import { Animated, Dimensions, View } from "react-native";
import { SafeAreaProvider, useSafeAreaInsets } from 'react-native-safe-area-context';import { NavigationContainer, DarkTheme, DefaultTheme } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { createStackNavigator } from '@react-navigation/stack'
const Stack = createStackNavigator();
import { Playlist } from './screens/Playlist';
import { Home } from './screens/Home';
import { PlaylistItem } from './PlaylistItem';
import { AllVideos } from './AllVideos';
import { SignUp } from './SignUp';
import { Account } from './Account';
import { Login } from './screens/Login';
import { UserProvider, useUserContext } from './navigation/UserContext';
import { ThemeProvider, useTheme } from './theme/ThemeContext';
import { Tags } from './Tags';

function HomeTabs() {
  const { colors } = useTheme()
  return (
    <Tab.Navigator tabBarOptions={{
      labelStyle: { fontSize: 12, textAlign: "center", fontWeight: "600", color: colors.text },
      tabStyle: { flex: 1, justifyContent: 'flex-end', height: 100, alignItems: "center" },
      indicatorStyle: {
          marginHorizontal: '5%',
          
          width: '30%'   
      },
      style: { backgroundColor: colors.tabBar },
    }} >
      <Tab.Screen name="Все видео" component={AllVideos} />
      <Tab.Screen name="Плейлисты" component={Home} />
      <Tab.Screen name="Аккаунт" component={AccountTabs} options={{
          headerShown: false,
        }} />
    </Tab.Navigator>
  );
}
const AccountTabs = () => {
  const {user} = useUserContext()
  return (
    <Stack.Navigator>
      {
        user && <Stack.Screen name="Account" component={Account} options={{
          headerShown: false,
        }} />
      }
      {
        !user && <Stack.Screen name="Login" component={Login} options={{
          headerShown: false,
        }}/>
      }
      <Stack.Screen name="Signup" component={SignUp} options={{
          headerShown: false,
        }} />
    </Stack.Navigator>
  )
}

const Tab = createMaterialTopTabNavigator();
function App() {
  const scheme = useColorScheme()
  return (
    <UserProvider>  
      <NavigationContainer theme={ scheme === "dark" ? DarkTheme : DefaultTheme } >
        <AppearanceProvider>
          <ThemeProvider>
            <Stack.Navigator mode="modal" >
      
      <Stack.Screen name="Home" component={HomeTabs} options={{
        headerShown: false,
      }} />
      <Stack.Screen name="Playlist" component={Playlist} />
      <Stack.Screen name="Tags" component={Tags} />
      <Stack.Screen name="PlaylistItem" component={PlaylistItem} />
      </Stack.Navigator>
          </ThemeProvider>
        </AppearanceProvider>
    </NavigationContainer>
      
    </UserProvider>
    
  );
}

const BGColor = "#transparent"
export default function SplashScreen({}) {

    // SafeArea Value...
    const edges = useSafeAreaInsets();

    // Animation Values....
    const startAnimation = useRef(new Animated.Value(0)).current;

    // Scaling Down Both logo and Title...
    const scaleLogo = useRef(new Animated.Value(1)).current;
    const scaleTitle = useRef(new Animated.Value(1)).current;

    // Offset Animation....
    const moveLogo = useRef(new Animated.ValueXY({ x: 0, y: 0 })).current;
    const moveTitle = useRef(new Animated.ValueXY({ x: 0, y: 0 })).current;

    // Animating COntent...
    const contentTransition = useRef(new Animated.Value(Dimensions.get('window').height)).current;

    // Animation Done....
    useEffect(() => {

        // Starting Animation after 500ms....
        setTimeout(() => {

            // Parallel Animation...
            Animated.parallel([
                Animated.timing(
                    startAnimation,
                    {
                        // For same Height for non safe Area Devices...
                        toValue: -Dimensions.get('window').height + (edges.top - 65),
                        useNativeDriver: true
                    }
                ),
                Animated.timing(
                    scaleLogo,
                    {
                        // Scaling to 0.35
                        toValue: 0.3,
                        useNativeDriver: true
                    }
                ),
                Animated.timing(
                    scaleTitle,
                    {
                        // Scaling to 0.8
                        toValue: 0.8,
                        useNativeDriver: true
                    }
                ),
                Animated.timing(
                    moveLogo,
                    {
                        // Moving to Right Most...
                        toValue: {
                            x: (Dimensions.get("window").width / 2) - 35,
                            y: (Dimensions.get('window').height / 2) - 5
                        },
                        useNativeDriver: true
                    }
                ),
                Animated.timing(
                    moveTitle,
                    {
                        // Moving to Right Most...
                        toValue: {
                            x: 0,
                            // Since image size is 100...
                            y: (Dimensions.get('window').height / 2) - 90
                        },
                        useNativeDriver: true
                    }
                ),
                Animated.timing(
                    contentTransition,
                    {
                        toValue: 0,
                        useNativeDriver: true
                    }
                )
            ])
                .start();

        }, 1000);

    }, [])
    const { colors } = useTheme()
    return (

        <View style={{
            position: 'absolute',
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
        }}>
            <Animated.View style={{
                flex: 1,
                backgroundColor: colors.background,
                zIndex: 1,
                transform: [
                    { translateY: startAnimation }
                ]
            }}>

                <Animated.View style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                }}>
                    <Animated.Image source={require("./assets/200px.png")} style={{
                        width: 100,
                        height: 100,
                        marginBottom: 20,
                        transform: [
                            { translateX: moveLogo.x },
                            { translateY: moveLogo.y },
                            { scale: scaleLogo },

                        ]
                    }}></Animated.Image>

                    <Animated.Text style={{
                        fontSize: 25,
                        fontWeight: 'bold',
                        color: '#000',
                        transform: [
                            { translateY: moveTitle.y },
                            { scale: scaleTitle }
                        ]
                    }}>Title placeholder</Animated.Text>

                </Animated.View>

            </Animated.View>

            <Animated.View style={{
                position: 'absolute',
                top: 0,
                bottom: 0,
                left: 0,
                right: 0,
                backgroundColor: colors.background,
                zIndex: 0,
                transform: [
                    { translateY: contentTransition }
                ]
            }}>
                <App/>
            </Animated.View>

        </View>
    );
}
function SplashContainer() {
  return (
    <SafeAreaProvider>
      <SplashScreen/>
    </SafeAreaProvider>
  )
}