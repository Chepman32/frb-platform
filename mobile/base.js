import firebase from "firebase"
export const app = firebase.initializeApp({
  apiKey: "AIzaSyD8ZNxAwFpYZ-d-g-tERAHcuCqT2ufpr_g",
  authDomain: "frb-platform.firebaseapp.com",
  projectId: "frb-platform",
  storageBucket: "frb-platform.appspot.com",
  messagingSenderId: "483838648511",
  appId: "1:483838648511:web:78063892e509d5fab8a60a",
  measurementId: "G-3P246S7X5C"
});
firebase.firestore().settings({experimentalForceLongPolling: true}); // add this
