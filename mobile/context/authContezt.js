import { createContext, useState, useEffect, useContext } from "react";
import { app } from "../base";
const AuthContext = createContext()
const useAuth = () => {
    return useContext(AuthContext)
}
const authProvider = ({children}) => {
    const [user, setUser] = useState(null)
    useEffect(() => {
        const unsubscribe = app.auth().onAuthStateChanged((user) => {
            if(user) {
                setUser(user)
            }
            else
            setUser(false)
        })
        return () => unsubscribe
    })
    const signIn = async (email, password) => {
        const {user} = await app.auth().signInWithEmailAndPassword(email, password).then((user) => {
            app.database().ref().child("users/").child(user.user.uid).on("value", (doc) => {
                setUser(user)
                return user
            })
        })
        .catch((err) => {
            alert(err)
        })
    }
    return (
       <AuthContext.Provider value={{user}} >
           {children}
       </AuthContext.Provider> 
    )
}
export{ authProvider, useAuth }