import React, { useEffect, useState } from "react"
import { StyleSheet, ScrollView, View } from "react-native"
import { useNavigation } from "@react-navigation/core"
import { app } from "./base"
import Preview from "./Preview"
import { useTheme } from "./theme/ThemeContext"
const db = app.firestore()
export const Home = () => {
  const [isReady, setIsReady] = useState(false)
  const [albums, setAlbums] = useState([])
  useEffect(() => {
    const unmount = db.collection("albums").onSnapshot((snapshot) => {
      const tempAlbums = []
      snapshot.forEach(doc =>{
        tempAlbums.push({...doc.data(), id: doc.id})
      })
      setAlbums(tempAlbums)
    })
    return unmount
  }, [])
  const {colors} = useTheme()
  const navigation = useNavigation()
  return (
    <ScrollView>
      <View style={{...styles.container, backgroundColor: colors.background}} >
      {albums.map((album) => <View key={album.id} >
      <Preview  info={album} callback={() => navigation.navigate("Playlist",
    {name: album.name, videos: album.videos})} />
      </View>)}
    </View>
    </ScrollView>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  cover: {
    width: "100%",
    height: 200,
  },
  text: {
    marginVertical: 40,
    fontSize: 30,
    fontWeight: "400"
  }
})