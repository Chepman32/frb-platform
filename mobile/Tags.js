import React, { useEffect } from "react"
import { useState } from "react"
import { FlatList, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native"
import { app } from "./base"
import { CustomTitle } from "./CustomTitle"
import Preview from "./Preview"
import { useTheme } from "./theme/ThemeContext"

const db = app.firestore()
export const Tags = ({navigation, route}) => {
  const [tagsData, setTagsData] = useState([]);
  const [videos, setVideos] = useState([])
  const [items, setItems] = useState([]);
  const [selectedId, setSelectedId] = useState(null);
  useEffect(() => {
    navigation.setOptions({
      header: (() => <CustomTitle name={""} />)
    })
  }, [])
  useEffect(() => {
    const unmount = db.collection("tags").onSnapshot((snapshot) => {
      const tempTags = [];
      snapshot.forEach((doc) => {
        tempTags.push(doc.data().name);
      });
      setTagsData(tempTags);
    });
    return unmount;
  }, [tag]);
  useEffect(() => {
    const unmount = db.collection("tags").doc(tag).onSnapshot((doc) => {
      setItems(doc.data().items)
    })
    return unmount
  }, [])
  useEffect(() => {
    const unmount = db.collection("albums").doc("All videos").onSnapshot((doc) => {
      setVideos(doc.data().videos)
      const tempVideos = doc.data().videos.filter((vid) => items.find((i) => i === vid.url))
      setVideos(tempVideos)
    })
    return unmount
  }, [items, tag, tagsData])
  const renderItem = ({item}) => (
    <Preview key={item.url} info={item} callback={() => navigation.navigate("PlaylistItem", {
      video: item,
      videos,
      autoPlay: true
    })} />
  )
  const { params } = route
  const { tag } = params
  const { colors } = useTheme()
  return (
    <SafeAreaView style={{...styles.container, backgroundColor: colors.background}}>
      <FlatList
        data={videos}
        renderItem={renderItem}
        keyExtractor={item => item.url}
        extraData={selectedId}
      />
    </SafeAreaView>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    marginVertical: 40,
    fontSize: 22,
    fontWeight: "400"
  },
  preview: {
    width: "20%",
    height: "15%"
  }
})