import React, { useEffect } from "react"
import { ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native"
import { CustomTitle } from "./CustomTitle"
import Preview from "./Preview"
import { useTheme } from "./theme/ThemeContext"

export const Playlist = ({navigation, route}) => {
  const {params} = route
  const { name } = params
  
  useEffect(() => {
    navigation.setOptions({
      header: (() => <CustomTitle name="" />)
    })
  }, [])
  const { colors } = useTheme()
  return (
    <ScrollView>
      <View style={{...styles.container, backgroundColor: colors.background}}>
      {
      params.videos.map((video) => <TouchableOpacity activeOpacity={0.8} key={video.name + Math.random().toString()}>
        <Preview info={video} callback={() =>navigation.navigate("PlaylistItem", {
          video,
          videos: params.videos,
          autoPlay: true
        })} />
      </TouchableOpacity>)
      }
    </View>
    </ScrollView>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    marginVertical: 40,
    fontSize: 22,
    fontWeight: "400"
  },
  preview: {
    width: "20%",
    height: "15%"
  }
})