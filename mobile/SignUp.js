import React, { useEffect, useState } from "react"
import { View, StyleSheet, TextInput, Text, Button, Alert, ActivityIndicator, Platform, Image, TouchableOpacity } from "react-native"
import firebase from "firebase"
import * as ImagePicker from 'expo-image-picker';
import 'react-native-get-random-values';
import { v4 as uuid } from 'uuid'
import { app } from "./base"
import { useTheme } from "./theme/ThemeContext";
import { height, width } from "./constants";
const db = app.firestore()
const storage = app.storage()
export const SignUp = ({navigation}) => {
    const [image, setImage] = useState(null);
    const [username, setUsername] = useState("JJJJJJJJJ")
    const [email, setEmail] = useState("lklkjlk@hhki.com")
    const [password, setPassword] = useState("123456")
    const [confirmed, setConfirmed] = useState("123456")
    const [isLoading, setisLoading] = useState(false)
    const [usernameError, setUsernameError] = useState("")
    useEffect(() => {
        (async () => {
          if (Platform.OS !== 'web') {
            const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
            if (status !== 'granted') {
              alert('Sorry, we need camera roll permissions to make this work!');
            }
          }
        })();
      }, []);
      async function uploadImageAsync(uri) {
        // Why are we using XMLHttpRequest? See:
        // https://github.com/expo/expo/issues/2402#issuecomment-443726662
        const blob = await new Promise((resolve, reject) => {
          const xhr = new XMLHttpRequest();
          xhr.onload = function() {
            resolve(xhr.response);
          };
          xhr.onerror = function(e) {
            console.log(e);
            reject(new TypeError('Network request failed'));
          };
          xhr.responseType = 'blob';
          xhr.open('GET', uri, true);
          xhr.send(null);
        });
      
        const ref = firebase
          .storage()
          .ref()
          .child(`avatars/9798798798789798798798789`);
        const snapshot = image && await ref.put(blob);
        blob.close();
  console.log(await snapshot.ref.getDownloadURL())
        return await snapshot.ref.getDownloadURL();
      }
      const pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.All,
          allowsEditing: true,
          aspect: [4, 3],
          quality: 1,
        });
        console.log(result);
        
        if (!result.cancelled) {
          setImage(result.uri);
        }
      };
    const registerUser = async () => {
        if(password === confirmed) {
          const storageRef = storage.ref()
          const response = await fetch(image)
        const blob = response.blob()
      const previewRef =  storageRef.child(`avatars/${uuid()}`)
      await previewRef.put(blob)
      db.collection("usersCollection").doc(email).set({
        favorites: [],
        history: [],
        comments: []
      }).then(async () => {
        setisLoading(true)
        db.collection("usersCollection").doc(email).update({photoURL: image && await uploadImageAsync(image)})
            app.auth().createUserWithEmailAndPassword(email, password).then( async (user) => {
                user.user.updateProfile({displayName: username, photoURL: image && await uploadImageAsync(image)})
                setisLoading(false)
                console.log(user.photoURL)
                    navigation.navigate("Account")
            })
      })
            .catch((err) => {
                alert(err)
                setisLoading(false)
            })
        }
        else Alert.alert("Passwords not match")
    }
    const { colors } = useTheme()
    return isLoading ? (
        <View style={styles.container}>
            <ActivityIndicator color="red"/>
        </View>
    )
    : (
        <View style={{...styles.container, backgroundColor: colors.background}}>
          
          <TouchableOpacity onPress={() => pickImage()}>
          <Image width={40} height={40} style={{width: height * 0.07, height: height * 0.07}} source={require("./assets/add-photo.png")}/>
          </TouchableOpacity>
            <Button title="Pick an image from camera roll" onPress={pickImage} />
            {image && <Image source={{ uri: image }} style={{ width: 200, height: 200 }} />}
            <TextInput style={{...styles.input, color: colors.text}} value={username} onChangeText={(text) => {
              if(text.length >= 10) {
                setUsernameError("Никнейм должен быть короче 10 символов")
              }
              else setUsernameError("")
              setUsername(text)
            }} placeholder="username"/>
            <Text style={{marginHorizontal: 30, color: "#f54251"}}>{usernameError} </Text>
            <TextInput style={{...styles.input, color: colors.text}} value={email} onChangeText={setEmail} placeholder="email"/>
            <TextInput style={{...styles.input, color: colors.text}} value={password} onChangeText={setPassword} placeholder="password"/>
            <TextInput style={{...styles.input, color: colors.text}} value={confirmed} onChangeText={setConfirmed} placeholder="confirm password"/>
            <Button title="Sign Up" onPress={() => usernameError ? alert(usernameError) : registerUser()} style={styles.btn}/>
            <Text style={{fontSize: height * 0.02, color: colors.text}} >Already have an account?&nbsp;<Text style={{fontSize: height * 0.02, color: "#f54251"}} onPress={() => navigation.navigate("Login")} >Login here</Text> </Text>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "flex-start",
        alignItems: "center"
    },
    imageContainer: {
      width: width * 0.9,
      marginVertical: height * 0.02,
      alignItems: "center",
      paddingHorizontal: 30,
      borderColor: "#66696e",
      borderRadius: 10,
    },
    input: {
        marginVertical: 30,
    },
    btn: {
        marginBottom: 30,
        backgroundColor: "red"
    }
})