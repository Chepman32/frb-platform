import React, { useState, useEffect }  from 'react';
import {
  View,
  SafeAreaView } from 'react-native';
import Carousel from 'react-native-snap-carousel';
import { app } from './base';
import { width } from './constants';
import RelativesItem from './RelativesItem';

const db = app.firestore()


export const Relatives = ({tags}) => {
  const [activeIndex, setActiveIndex] = useState(0)
  const [videos, setVideos] = useState([])
  useEffect(() => {
    const unmount = db.collection("albums").doc("All videos").onSnapshot((doc) => {
      const filtered = []
      for(let i = 0; i< 10; i++) {
        filtered.push(videos[getRandomInt(0, videos.length)])
      }
      filtered && setVideos(doc.data().videos || [])
      console.log(filtered)
    })
    return unmount
  }, [])
  function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
  const renderItem = ({item}) => {
    
      return (
        <RelativesItem info={item} videos={videos} />

      )
  }
  let carousel = null
  return (
    <SafeAreaView style={{flex: 1, backgroundColor:'transparent', paddingTop: 50, }}>
      <View style={{ flex: 1, flexDirection:'row', justifyContent: 'center', }}>
          <Carousel
            layout={"default"}
            ref={ref => carousel = ref}
            data={videos}
            sliderWidth={width}
            itemWidth={300}
            renderItem={renderItem}
            onSnapToItem = { index => setActiveIndex(index) } />
      </View>
    </SafeAreaView>
  );
}