import React, { useState } from 'react'
import { View, Image, Text, StyleSheet } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { height, width } from './constants'
import { useTheme } from './theme/ThemeContext'

export default function Preview({info, callback}) {
  const {colors} = useTheme()
  return (
    <TouchableOpacity onPress={callback} style={{...styles.container, flexDirection: info.cover ? "row" : "column", backgroundColor: colors.background}} activeOpacity={0.8} >
      <Image source={{uri: info.cover || info.previewUrl}} style={info.cover ? styles.placeholder : styles.logo} />
      <Text style={info.cover ? {...styles.smallText, color: colors.text} : {...styles.text, color: colors.text}}>{info.name} </Text>
    </TouchableOpacity>
  )
}
const styles = StyleSheet.create({
  container: {
    width,
    marginVertical: 20,
    justifyContent: "center",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 20,
    position: "relative"
  },
  placeholder: {
    width: width * 0.5,
    height: height * 0.17,
  },
  logo: {
    width,
    height: height * 0.3,
  },
  text: {
    width: width * 0.9,
    marginTop: 20,
    fontSize: 20,
    fontWeight: "400",
    textAlign: "center"
  },
  smallText: {
    width: width * 0.5,
    marginTop: 20,
    fontSize: 15,
    fontWeight: "400",
    textAlign: "center"
  },
})