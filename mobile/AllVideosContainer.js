import React, { useEffect, useState } from "react"
import { StyleSheet } from "react-native"
import { app } from "./base"
import { AllVideos } from "./AllVideos"
const db = app.firestore()

export const AllVideosContainer = ({navigation}) => {
  const [albums, setAlbums] = useState([])
  const [videos, setVideos] = useState([])
  useEffect(() => {
    const unmount = db.collection("albums").onSnapshot((snapshot) => {
      const tempAlbums = []
      snapshot.forEach(doc =>{
        tempAlbums.push({...doc.data(), id: doc.id})
      })
      setAlbums(tempAlbums)
      getAll()
    })
  })
  const getAll = () => {
    let tempVideos = []
    for(let i = 1; i < albums.length; i++) {
      tempVideos.push(...albums[i].videos)
      setVideos(tempVideos)
      tempVideos && console.log(videos.length) 
    }
  }
  return (
    <AllVideos videos={videos}/>
  )
}
const styles = StyleSheet.create({
  container: {
    width: "100%",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 50,
}
})