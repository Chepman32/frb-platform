import React, { useEffect } from "react"
import { useState } from "react"
import { ScrollView, StyleSheet, FlatList, TouchableOpacity, View, SafeAreaView } from "react-native"
import { app } from "./base"
import Preview from "./Preview"
import { useTheme } from "./theme/ThemeContext"

const db = app.firestore()
export const AllVideos = ({navigation}) => {
  const [videos, setVideos] = useState([])
  const [selectedId, setSelectedId] = useState(null);
  useEffect(() => {
    const unmount = db.collection("albums").doc("All videos").onSnapshot((doc) => {
      doc.data().videos && setVideos(doc.data().videos || [])
    })
    return unmount
  }, [])
  const { colors } = useTheme()
  const renderItem = ({item}) => (
    <Preview key={item.url} info={item} callback={() => navigation.navigate("PlaylistItem", {
      video: item,
      videos,
      autoPlay: true
    })} />
  )
  return (
    <SafeAreaView style={{...styles.container, backgroundColor: colors.background}}>
      <FlatList
        data={videos}
        renderItem={renderItem}
        keyExtractor={item => item.url}
        extraData={selectedId}
      />
    </SafeAreaView>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    marginVertical: 40,
    fontSize: 22,
    fontWeight: "400"
  },
  preview: {
    width: "20%",
    height: "15%"
  }
})