import React, { useEffect, useState } from "react"
import { View, StyleSheet, Text, Button, TouchableOpacity } from "react-native"
import { AccountInfo } from "./AccountInfo"
import { app } from "./base"
const db = app.firestore()
import { height, width } from "./constants"
import { useUserContext } from "./navigation/UserContext"
import { useTheme } from "./theme/ThemeContext"
export const Account = ({navigation}) => {
    const [favorites, setFavorites] = useState([])
    const [history, setHistory] = useState([])
    useEffect(() => {
        const unmount = db.collection("usersCollection").doc(user.email)
        .onSnapshot((doc) => {
            doc.data().favorites && setFavorites(doc.data().favorites)
        });
        return unmount
      }, [])
      useEffect(() => {
        const unmount = db.collection("usersCollection").doc(user.email)
        .onSnapshot((doc) => {
            doc.data().history && setHistory(doc.data().history)
        });
        return user && unmount
      }, [])
    const { user } = useUserContext()
    const { colors } = useTheme()
    return (
        <View style={{...styles.container, backgroundColor: colors.background}}>
            <AccountInfo/>
            <TouchableOpacity onPress={() => navigation.navigate("Playlist",
    {name: "История", videos: history})} style={styles.menuItem} >
                <Text style={{...styles.menuItemText, color: colors.text}}>История</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate("Playlist",
    {name: "Избранное", videos: favorites})} style={styles.menuItem} >
                <Text style={{...styles.menuItemText, color: colors.text}}>Избранное</Text>
            </TouchableOpacity>
            <Button title="Sign out" onPress={() => app.auth().signOut()} color={colors.text} style={styles.btn}/>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        paddingBottom: height * 0.1,
        flex: 1,
        justifyContent: "space-between",
        alignItems: "center"
    },
    menuItem: {
        width,
        padding: 20,
        flexDirection: "row",
        borderWidth: 1,
        borderTopColor: "#cc1"
    },
    menuItemText: {
        fontSize: 20,
        fontWeight: "400",
    },
    btn: {
        marginBottom: 30,
    }
})