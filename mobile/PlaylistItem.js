import React, { useEffect, useState } from "react"
import { View, Button, Text } from "react-native";
import { app } from "./base";
import ExpoPlayer from "./ExpoPlayer";
const db = app.firestore()
export const PlaylistItem = ({navigation, route}) => {
  const [url, setUrl] = useState("")
  const [current, setCurrent] = useState()
  const [loaded, setLoaded] = useState(false)
  const [tags, setTags] = useState([])
  useEffect(() => {
    const unmount = db.collection("tags").onSnapshot((snapshot) => {
      const tempTags = []
      snapshot.forEach(doc =>{
        tempTags.push({...doc.data(), name: doc.data().name, items: doc.data().items})
      })
      const filtered = tempTags.filter(t => t.items.includes(video.url))
      setTags(filtered)
    })
    return unmount
  }, [])
  const setNext = () => {
    videos.indexOf(video) < videos.length - 1 && navigation.push("PlaylistItem", {
      videos,
      video: videos[videos.indexOf(video) + 1]
    })
    videos.indexOf(video) === videos.length - 1 && navigation.push("PlaylistItem", {
      videos,
      video: videos[0],
      autoPlay: true
    })
  }
  const setPrev = () => {    
    videos.indexOf(video) !== 0 && navigation.push("PlaylistItem", {
      videos,
      video: videos[videos.indexOf(video) - 1]
    })
    videos.indexOf(video) === 0 && navigation.push("PlaylistItem", {
      videos,
      video: videos[videos.length - 1]
    })
  }
  const {params} = route
  const { video, videos, autoPlay } = params
if(!loaded) {
  <Text>Loading</Text>
}
  return (
    <ExpoPlayer videos={videos} tags={video.tags} uri={video.url} playPrev={setPrev} playNext={setNext} autoPlay={autoPlay} title={video.name} desc={video.desc} posterSrc={video.previewUrl} />
  )
}