import React, { useState, createContext, useContext, useEffect } from 'react';
import { app } from '../base';

export const UserContext = createContext({initValues});
const initValues= {user: null, authenticated: false}
const db = app.firestore()

export const UserProvider = ({ children }) => {
  const [user, setUser] = useState(null);
  const [photoUrl, setPhotoUrl] = useState("")
  const [favorites, setFavorites] = useState([])
    const [history, setHistory] = useState([])
  const [isAuthenticated, setIsAuthenticated] = useState(false)
  useEffect(() => {
    return app.auth().onAuthStateChanged((user) => {
      if(user) {
        setUser(user)
        setIsAuthenticated(true)

      }
      else {
        setUser(null)
        setIsAuthenticated(false)
      }
    })
  })
  useEffect(() => {
    if(user) {
      getHistory()
      getFav()
    }
  }, [user])
  function getHistory() {
    try {
      app.firestore().collection("usersCollection").doc(user.email).get().then(async queryResult =>{
        const alldata = queryResult.data().history
        setHistory(alldata)
       })
    }
    catch(e) {
      console.log("No user")
    }
  }
function getFav() {
    try {
      app.firestore().collection("usersCollection").doc(user.email).get().then(async queryResult =>{
        const alldata = queryResult.data().favorites
        setFavorites(alldata)
       })
    }
    catch(e) {
      console.log("No user")
    }
  }
  return (
    <UserContext.Provider value={{ user, isAuthenticated, history, favorites }}>
      {children}
    </UserContext.Provider>
  );
};
export const useUserContext = () => useContext(UserContext)